/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package ft;

import ft.common.constants.ConfigurationConstants;

import ft.common.controller.DatabaseController;
import ft.common.controller.FileController;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.log4j.PropertyConfigurator;

import java.io.File;

import java.net.URL;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class Application
{
	//~ Static fields/initializers ---------------

	private static Log FTLogger = LogFactory.getLog(Application.class);

	//~ Constructors -----------------------------

	/** Creates a new Application object. */
	public Application( )
	{
		URL url = getClass().getResource("/" + ConfigurationConstants.LOG_CONFIG);
		PropertyConfigurator.configure(url);
	}

	//~ Methods ----------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param  args  DOCUMENT ME!
	 */
	public static void main(String[] args)
	{
		Application app = new Application();
		FTLogger.info("Security Application Started");
		app.executeTask();
		FTLogger.info("Security Application Completed!");
	}

	//~ ------------------------------------------

	/** DOCUMENT ME! */
	public void executeTask()
	{
		FTLogger.info("Executing Task...");
		FileController.initFileController();

		Date curDate = new Date();
		FTLogger.info("========================================");
		FTLogger.info("Removing Expired Access Cards...");
		FTLogger.info("Executution Date : " + curDate);

		int cnt = DatabaseController.cleanUpSecurityInfo(curDate);
		FTLogger.info("Record[s] removed : " + cnt);
		FTLogger.info("========================================");

		File[] files = FileController.getFileList(FileController.getSourceDirectory());

		if (files.length == 0)
		{
			FTLogger.info("There is no file to process!");
		}

		for (int i = 0; i < files.length; i++)
		{
			boolean isValid = false;
			String fName = files[i].getName();

			List<String> filePrefixes = FileController.getFilePrefixList();
			Iterator<String> iterFilePrefix = filePrefixes.iterator();
			while (iterFilePrefix.hasNext())
			{
				String prefix = iterFilePrefix.next();
				if (fName.startsWith(prefix))
				{
					isValid = true;

					break;
				}
			}

			if (!isValid)
			{
				FTLogger.info("========================================");
				FTLogger.info(fName + " is not a valid file!");
				FTLogger.info("========================================");
				FileController.moveFile(files[i].getName(), true);

				continue;
			}

			List<String> fileDetails = FileController.readFromFile(files[i]);
			if ((fileDetails != null) && (fileDetails.size() > 0))
			{
				FTLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
				FTLogger.info("Processing for " + fName + " started at : " + new Date());
				FTLogger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

				int ctr = 0;
				Iterator<String> iterFileDetails = fileDetails.iterator();
				while (iterFileDetails.hasNext())
				{
					String row = null;

					try
					{
						ctr++;
						FTLogger.info("Processing " + ctr + " of " + fileDetails.size() + "...");

						row = iterFileDetails.next();
						if ((row != null) && (row.trim().length() > 0))
						{
							String[] details = row.split(FileController.getDelimiter());
							DatabaseController.updateSecurityInfo(details);
						}
					}
					catch (Exception e)
					{
						FTLogger.error("Invalid row data. row no: " + ctr + " data :" + row);
					}
				}

				FileController.moveFile(files[i].getName());
				FTLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				FTLogger.info("Processing for " + files[i].getName() + " completed at : "
					+ new Date());
				FTLogger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}
		} // end for

		FTLogger.info("Task Completed!");
	}
}
