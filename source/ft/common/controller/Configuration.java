package ft.common.controller;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Configuration {
	private static final String RESOURCE_BUNDLE_NAME = "ft.common.resource.Configuration";
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
		.getBundle(RESOURCE_BUNDLE_NAME, Locale.getDefault());
	
	private Configuration() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return key + " : Does not Exist";
		}
	}
	
	public static int getValue(String key) {
		try {
			return Integer.parseInt(RESOURCE_BUNDLE.getString(key));
		} catch (MissingResourceException m) {
			return 0;
		} catch (Exception e) {
			return 0;
		}
	}
}
