package ft.common.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateController {
	private static SimpleDateFormat fullDate12Hr = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
	
	public static final String MAX_DATE = "31/12/9999 11:59:59 pm";
	public static final String MIN_DATE = "01/01/1970 12:00:00 am";
	
	public static Date getFullDate12Hr(String dateStr)
	{
		Date date = null;
		
		try {
			date = fullDate12Hr.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
}
