/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package ft.common.controller;

import ft.common.constants.ConfigurationConstants;
import ft.common.constants.Constants;
import ft.common.constants.DatabaseConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Date;
import java.util.List;
import java.util.Properties;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class DatabaseController
{
	//~ Static fields/initializers ---------------

	private static Log FTLogger = LogFactory.getLog(DatabaseController.class);

	private static Date MIN_DATE = DateController.getFullDate12Hr(DateController.MIN_DATE);
	private static Date MAX_DATE = DateController.getFullDate12Hr(DateController.MAX_DATE);

	private static int connection_retry_count = 0;

	//~ Methods ----------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param   date  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static int cleanUpSecurityInfo(Date date)
	{
		FTLogger.info("Security Information Cleanup Started");

		int affectedRow = 0;

		Connection conn = null;
		PreparedStatement prepStmt = null;
		ResultSet resultSet = null;
		try
		{
			conn = getConnection();

			prepStmt = conn.prepareStatement("UPDATE " + DatabaseConstants.USER_INFO_TABLE + " SET "
					+ DatabaseConstants.USER_INFO_EMP_NUM + " =(?),"
					+ DatabaseConstants.USER_INFO_AUTH_TYPE + " =(?),"
					+ DatabaseConstants.USER_INFO_PASSWORD + " =(?),"
					+ DatabaseConstants.USER_INFO_CARD_NUM + " =(?),"
					+ DatabaseConstants.USER_INFO_REG_DATE + " =(?),"
					+ DatabaseConstants.USER_INFO_EXP_DATE + " =(?)" + " WHERE "
					+ DatabaseConstants.USER_INFO_EXP_DATE + " <= (?)");

			prepStmt.setString(1, "");
			prepStmt.setString(2, Constants.AUTH_TYPE_PWORD);
			prepStmt.setString(3, Constants.DEFAULT_PWORD);
			prepStmt.setString(4, "");
			prepStmt.setTimestamp(5, new java.sql.Timestamp(MIN_DATE.getTime()));
			prepStmt.setTimestamp(6, new java.sql.Timestamp(MAX_DATE.getTime()));
			prepStmt.setTimestamp(7, new java.sql.Timestamp(date.getTime()));

			affectedRow = prepStmt.executeUpdate();
		}
		catch (Exception e)
		{
			FTLogger.error(e.toString());
			e.printStackTrace();
		}
		finally
		{
			closeQuery(resultSet, prepStmt, conn);
		}

		FTLogger.info("Security Information Cleanup Completed");

		return affectedRow;
	}

	//~ ------------------------------------------
	private static Connection con = null;
	public static Connection getConnection()
	{
		String driverName = Configuration.getString(ConfigurationConstants.DB_DRIVER);
		String connectStr = Configuration.getString(ConfigurationConstants.DB_URL);
		String uName = Configuration.getString(ConfigurationConstants.DB_USER);
		String pWord = Configuration.getString(ConfigurationConstants.DB_PASS);
		String schema = Configuration.getString(ConfigurationConstants.DB_SCHEMA);

		if(con==null)
		con = createConnection(driverName, connectStr, uName, pWord, schema);
		return con;
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param  securityInfo  DOCUMENT ME!
	 */
	public static void updateSecurityInfo(String[] securityInfo)
	{
		/* Index 0 = Card Number
		 *       1 = Card Holder ID (Used to Mapped the ID to RF Card)      2 = Active Flag (0 =
		 * Inactive, 1 = Active)      3 = Creation Date      4 = Expiration Date      5 = Name
		 * 6 = Group      7 = Others(NRIC)      8 = Record Count
		 * */

		boolean isExist = false;

		String cardNumber = securityInfo[0];
		String empNumber = securityInfo[1];
		// String activeFlag = securityInfo[2];
		String createDateStr = securityInfo[3];
		String expiryDateStr = securityInfo[4];
		String staffName = securityInfo[5];
		String groupName = securityInfo[6];

		List<String> ignoreList = FileController.getIgnoreList();
		if ((ignoreList != null) && (ignoreList.size() > 0))
		{
			if (ignoreList.contains(groupName))
			{
				FTLogger.error("Skipping row for " + staffName + " with group " + groupName
					+ "...");

				return;
			}
		}

		FTLogger.info("Security Information Update for : " + staffName);

		if (staffName.length() > 45)
		{
			staffName = staffName.substring(0, 44);
		}

		Date createDate = DateController.getFullDate12Hr(createDateStr);
		Date expiryDate = DateController.getFullDate12Hr(expiryDateStr);

		if (expiryDate == null)
		{
			expiryDate = DateController.getFullDate12Hr(DateController.MAX_DATE);
		}

		Connection conn = null;
		PreparedStatement prepStmt = null;
		ResultSet resultSet = null;
		Date regDate = null;
		if(empNumber!=null)
		{
			//999070007
			//to 39070007
			if(empNumber.length()==9)
			{
				String firstTwo = empNumber.substring(0, 2);
				if(firstTwo.equals("99"))
				{
					System.out.println("ft:"+firstTwo);
					empNumber = "3"+empNumber.substring(2, 9);
				}
			}

		}
		try
		{
			conn = getConnection();
			prepStmt = conn.prepareStatement("SELECT " + DatabaseConstants.USER_INFO_EMP_NUM + ", "
					+ DatabaseConstants.USER_INFO_REG_DATE + " FROM "
					+ DatabaseConstants.USER_INFO_TABLE + " WHERE "
					+ DatabaseConstants.USER_INFO_EMP_NUM + " = (?)");

			prepStmt.setString(1, empNumber);

			prepStmt.executeQuery();
			resultSet = prepStmt.executeQuery();
			while (resultSet.next())
			{
				regDate = resultSet.getDate(DatabaseConstants.USER_INFO_REG_DATE);
				isExist = true;

				break;
			}

			if (isExist)
			{
				FTLogger.info("Update User Info...");

				if (regDate.after(createDate))
				{
					FTLogger.error("Creation Date is older than the one in the existing record!");

					return;
				}

				prepStmt = conn.prepareStatement("UPDATE " + DatabaseConstants.USER_INFO_TABLE
						+ " SET " + DatabaseConstants.USER_INFO_CARD_NUM + " = (?),"
						+ DatabaseConstants.USER_INFO_NAME + " = (?),"
						+ DatabaseConstants.USER_INFO_DEPARTMENT + " = (?),"
						//commented out 05-Aug-2015. No need to update the authType for existing users so that FP/RF authType will not be overwritten.
						//+ DatabaseConstants.USER_INFO_AUTH_TYPE + " = " + Constants.AUTH_TYPE_RFID + "," 
						+ DatabaseConstants.USER_INFO_REG_DATE + " = (?),"
						+ DatabaseConstants.USER_INFO_EXP_DATE + " = (?)" + " WHERE "
						+ DatabaseConstants.USER_INFO_EMP_NUM + " = (?)");

				prepStmt.setString(1, cardNumber);
				prepStmt.setString(2, staffName);
				prepStmt.setString(3, groupName);
				prepStmt.setTimestamp(4, new java.sql.Timestamp(createDate.getTime()));
				prepStmt.setTimestamp(5, new java.sql.Timestamp(expiryDate.getTime()));
				prepStmt.setString(6, empNumber);
				prepStmt.executeUpdate();
			}
			else
			{
				FTLogger.info("Adding User Info...");

				if (expiryDate.after(new Date()))
				{
					boolean isMaxRecord = true;

					prepStmt = conn.prepareStatement("SELECT " + DatabaseConstants.USER_INFO_ID
							+ " FROM " + DatabaseConstants.USER_INFO_TABLE + " WHERE "
							+ DatabaseConstants.USER_INFO_EMP_NUM + " = ''");

					resultSet = prepStmt.executeQuery();
					while (resultSet.next())
					{
						isMaxRecord = false;

						String recId = resultSet.getString(DatabaseConstants.USER_INFO_ID);
						prepStmt = conn.prepareStatement("UPDATE "
								+ DatabaseConstants.USER_INFO_TABLE + " SET "
								+ DatabaseConstants.USER_INFO_CARD_NUM + " = (?),"
								+ DatabaseConstants.USER_INFO_NAME + " = (?),"
								+ DatabaseConstants.USER_INFO_EMP_NUM + " = (?),"
								+ DatabaseConstants.USER_INFO_DEPARTMENT + " = (?),"
								+ DatabaseConstants.USER_INFO_AUTH_TYPE + " = "
								+ Constants.AUTH_TYPE_RFID + ","
								+ DatabaseConstants.USER_INFO_REG_DATE + " = (?),"
								+ DatabaseConstants.USER_INFO_EXP_DATE + " = (?)" + " WHERE "
								+ DatabaseConstants.USER_INFO_ID + " = (?)");

						prepStmt.setString(1, cardNumber);
						prepStmt.setString(2, staffName);
						prepStmt.setString(3, empNumber);
						prepStmt.setString(4, groupName);
						prepStmt.setTimestamp(5, new java.sql.Timestamp(createDate.getTime()));
						prepStmt.setTimestamp(6, new java.sql.Timestamp(expiryDate.getTime()));
						prepStmt.setString(7, recId);
						prepStmt.executeUpdate();

						break;
					}

					if (isMaxRecord)
					{
						FTLogger.error("Max Record Already Reached!!!");
						FTLogger.error("Please Add more slots or free up some records.");
					}
				}
				else
				{
					FTLogger.info("Card(" + cardNumber + ") is Already Expired!!!");
				} // end if-else
			} // end if-else
		}
		catch (Exception e)
		{
			FTLogger.error(e.toString());
			e.printStackTrace();
		}
		finally
		{
			closeQuery(resultSet, prepStmt, conn);
		} // end try-catch-finally

		FTLogger.info("Security Information Update for " + staffName + " completed!");
	}

	//~ ------------------------------------------

	private static final void closeConnection(Connection con)
	{
		if (con != null)
		{
			try
			{
				con.commit();
				con.close();
				con = null;
			}
			catch (SQLException e)
			{
				FTLogger.error("Exception during closeing statement");
				e.printStackTrace();
			}
		}
	}

	//~ ------------------------------------------

	private static final synchronized void closeQuery(ResultSet rs, Statement stmt, Connection con)
	{
		closeResultSet(rs);
		closeStatement(stmt);
//		closeConnection(con);
	}

	//~ ------------------------------------------

	private static final void closeResultSet(ResultSet rs)
	{
		if (rs != null)
		{
			try
			{
				rs.close();
				rs = null;
			}
			catch (SQLException e)
			{
				FTLogger.error("Exception when trying close result set");
				e.printStackTrace();
			}
		}
	}

	//~ ------------------------------------------

	private static final void closeStatement(Statement stmt)
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
				stmt = null;
			}
			catch (SQLException e)
			{
				FTLogger.error("Exception during closeing statement");
				e.printStackTrace();
			}
		}
	}

	//~ ------------------------------------------

	private static Connection createConnection(String driverName, String connectStr,
		String uName, String pWord, String schema)
	{
		Connection conn = null;

		Properties p = new Properties();
		p.setProperty("user", uName);
		p.setProperty("password", pWord);

		if ((schema != null) && (schema.trim().length() > 0))
		{
			p.setProperty("currentSchema", schema);
		}

		try
		{
			if (conn == null)
			{
				Class.forName(driverName);
				conn = DriverManager.getConnection(connectStr, p);
			}

			if (conn == null)
			{
				throw new Exception();
			}

			connection_retry_count = 0;
		}
		catch (Exception e)
		{
			FTLogger.error("Error getting connection.");
			FTLogger.error(e.getMessage());
			connection_retry_count++;
			if (connection_retry_count <= 5)
			{
				FTLogger.error("Retrying...");
				try
				{
					Thread.sleep(5000);
				}
				catch (InterruptedException ie)
				{
					ie.printStackTrace();
				}
				FTLogger.error("Retry Count : " + connection_retry_count);
				conn = createConnection(driverName, connectStr, uName, pWord, schema);
			}
		}

		return conn;
	}
}
