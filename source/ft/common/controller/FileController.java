/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package ft.common.controller;

import ft.common.constants.ConfigurationConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class FileController
{
	//~ Static fields/initializers ---------------

	private static Log FTLogger = LogFactory.getLog(FileController.class);
	private static String SOURCE_DIR = null;
	private static String PROCESSED_DIR = null;
	private static String ERROR_DIR = null;
	private static String DELIMITER = null;
	private static List<String> FILE_PREFIX_LIST = null;
	private static List<String> IGNORE_LIST = null;

	private static FilenameFilter fileFilter = new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				String fileExt = name.substring(name.lastIndexOf("."));
				if ((fileExt != null)
					&& fileExt.equalsIgnoreCase(
						Configuration.getString(ConfigurationConstants.FILE_EXTENSION)))
				{
					return true;
				}

				return false;
			}
		};

	//~ Methods ----------------------------------

	public static String getDelimiter()
	{
		return DELIMITER;
	}

	//~ ------------------------------------------

	public static String getErrorDirectory()
	{
		return ERROR_DIR;
	}

	//~ ------------------------------------------

	public static File[] getFileList(String dirStr)
	{
		File dir = new File(dirStr);

		return dir.listFiles(fileFilter);
	}

	//~ ------------------------------------------

	public static List<String> getFilePrefixList()
	{
		return FILE_PREFIX_LIST;
	}

	//~ ------------------------------------------

	public static List<String> getIgnoreList()
	{
		return IGNORE_LIST;
	}

	//~ ------------------------------------------

	public static String getProcessedDirectory()
	{
		return PROCESSED_DIR;
	}

	//~ ------------------------------------------

	public static String getSourceDirectory()
	{
		return SOURCE_DIR;
	}

	//~ ------------------------------------------

	/** DOCUMENT ME! */
	public static void initFileController()
	{
		FTLogger.info("Initializing File Directories...");

		SOURCE_DIR = Configuration.getString(ConfigurationConstants.SOURCE_DIRECTORY);
		PROCESSED_DIR = Configuration.getString(ConfigurationConstants.PROCESSED_DIRECTORY);
		ERROR_DIR = Configuration.getString(ConfigurationConstants.ERROR_DIRECTORY);
		DELIMITER = Configuration.getString(ConfigurationConstants.FIELD_DELIMITER);

		String prefixes = Configuration.getString(ConfigurationConstants.FILE_PREFIX);
		String[] prefixGrp = prefixes.split(";");
		if (prefixGrp.length > 0)
		{
			FILE_PREFIX_LIST = Arrays.asList(prefixGrp);
		}

		String ignoreGrpStr = Configuration.getString(ConfigurationConstants.IGNORE_GROUP);
		String[] ignoreGrp = ignoreGrpStr.split(";");
		if (ignoreGrp.length > 0)
		{
			IGNORE_LIST = Arrays.asList(ignoreGrp);
		}

		File sourceDir = new File(SOURCE_DIR);
		if (!sourceDir.exists())
		{
			FTLogger.info("Creating Source Directory...");
			sourceDir.mkdirs();
		}

		File processedDir = new File(PROCESSED_DIR);
		if (!processedDir.exists())
		{
			FTLogger.info("Creating Processed Directory...");
			processedDir.mkdirs();
		}

		File errorDir = new File(ERROR_DIR);
		if (!errorDir.exists())
		{
			FTLogger.info("Creating Error Directory...");
			errorDir.mkdirs();
		}

		FTLogger.info("Initialization Complete!");
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param  fname  DOCUMENT ME!
	 */
	public static void moveFile(String fname)
	{
		moveFile(fname, false);
	}

	//~ ------------------------------------------

	/**
	 * DOCUMENT ME!
	 *
	 * @param  fname  DOCUMENT ME!
	 */
	public static void moveFile(String fname, boolean isError)
	{
		File srcFile = new File(SOURCE_DIR + "\\" + fname);

		Date curDate = new Date();
		String name = fname.substring(0, fname.indexOf("."));
		String ext = fname.substring(fname.indexOf(".") + 1);

		StringBuilder builder = new StringBuilder();

		if (isError)
		{
			builder.append(ERROR_DIR);
			FTLogger.info("Moving " + fname + " to " + ERROR_DIR + "...");
		}
		else
		{
			builder.append(PROCESSED_DIR);
			FTLogger.info("Moving " + fname + " to " + PROCESSED_DIR + "...");
		}

		builder.append(System.getProperty("file.separator")).append(name).append("_").append(curDate
			.getTime()).append(".").append(ext);

		File destFile = new File(builder.toString());

		if (srcFile.renameTo(destFile))
		{
			FTLogger.info("File has been moved successfully!");
		}
		else
		{
			FTLogger.error("Unable to rename/move " + fname);
		}
	}

	//~ ------------------------------------------

	/**
	 * public static String[] getFileListStr(String dirStr){ File dir = new File(dirStr); return
	 * dir.list(fileFilter); }
	 *
	 * @param   file  DOCUMENT ME!
	 * @return  DOCUMENT ME!
	 */
	public static List<String> readFromFile(File file)
	{
		List<String> details = null;
		Scanner scanner = null;
		try
		{
			if ((file == null) || !file.exists())
			{
				FTLogger.error("File does not Exist!");

				return details;
			}

			int skipRow = Configuration.getValue(ConfigurationConstants.SKIPPED_ROW_COUNT);
			int ignoreRow = Configuration.getValue(ConfigurationConstants.IGNORE_ROW_COUNT);
			int ctr = 0;
			details = new ArrayList<String>();
			FTLogger.info("Parsing file " + file.getName() + "...");
			scanner = new Scanner(new FileInputStream(file));
			while (scanner.hasNextLine())
			{
				String row = scanner.nextLine();
				if (ctr != skipRow)
				{
					ctr++;
					FTLogger.info("Skipping Row " + ctr);

					continue;
				}
				details.add(row);
			}

			for (int i = 0; i < ignoreRow; i++)
			{
				int lastRow = details.size() - 1;
				FTLogger.info("Removing Row " + lastRow);
				details.remove(lastRow);
			}
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			scanner.close();
		} // end try-catch-finally

		return details;
	}
}
