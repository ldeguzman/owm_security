/*
 * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of FriarTuck.
 * ("Confidential Information").  You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FriarTuck.
 *
 * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
package ft.common.constants;

/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class ConfigurationConstants
{
	//~ Static fields/initializers ---------------

	/** DOCUMENT ME! */
	public static final String LOG_CONFIG = "log4j.properties";

	/** File Configuration */
	public static final String SKIPPED_ROW_COUNT = "skippedRowCnt";

	/** DOCUMENT ME! */
	public static final String IGNORE_ROW_COUNT = "ignoreRowCnt";

	/** DOCUMENT ME! */
	public static final String FILE_EXTENSION = "fileExt";

	/** DOCUMENT ME! */
	public static final String SOURCE_DIRECTORY = "srcDir";

	/** DOCUMENT ME! */
	public static final String PROCESSED_DIRECTORY = "processedDir";

	/** DOCUMENT ME! */
	public static final String ERROR_DIRECTORY = "errorDir";

	/** DOCUMENT ME! */
	public static final String FILE_PREFIX = "filePrefix";

	/** DOCUMENT ME! */
	public static final String FIELD_DELIMITER = "delimiter";

	/** Database Configuration */

	/*Database Config*/
	public static final String DB_URL = "DatabaseURL";

	/** DOCUMENT ME! */
	public static final String DB_USER = "DatabaseUser";

	/** DOCUMENT ME! */
	public static final String DB_PASS = "DatabasePassword";

	/** DOCUMENT ME! */
	public static final String DB_DRIVER = "DatabaseDriver";

	/** DOCUMENT ME! */
	public static final String DB_SCHEMA = "DatabaseSchema";

	/** Other Configuration */
	public static final String IGNORE_GROUP = "IgnoreGroup";
}
