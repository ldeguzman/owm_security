package ft.common.constants;

public class DatabaseConstants {

	/*User Info Table*/
	public static final String  USER_INFO_TABLE = "NGAC_USERINFO";
	public static final String  USER_INFO_ID = "ID";
	public static final String  USER_INFO_PASSWORD = "Password";
	public static final String  USER_INFO_AUTH_TYPE = "AuthType";
	
	public static final String  USER_INFO_NAME = "Name";
	public static final String  USER_INFO_EMP_NUM = "EmployeeNum";
	public static final String  USER_INFO_DEPARTMENT = "Department";
	public static final String  USER_INFO_REG_DATE = "regDate";
	public static final String  USER_INFO_EXP_DATE = "expDate";
	public static final String  USER_INFO_CARD_NUM = "RFID";
}
